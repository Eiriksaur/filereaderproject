package loggservice;

import java.time.format.DateTimeFormatter;  
import java.time.LocalDateTime;
import java.io.FileWriter;
import java.io.IOException;

public class Logger{
    //A class mad for simple logging of messages and how long the functions generating them took to complete.

    FileWriter fileWriter;

    public void logg(long exeTime, String loggString) throws Exception, IOException{
        try{
            DateTimeFormatter date = DateTimeFormatter.ofPattern("dd/MM/yy HH:mm:ss");  //Setting the pattern for the date thats used in the logg.txt
            LocalDateTime localDate = LocalDateTime.now();
            fileWriter = new FileWriter("../src/main/loggs/logg.txt", true);
            fileWriter.write(date.format(localDate) + " --> " + loggString + " --> Time to execute: " + exeTime +"ms \n");
        } catch(IOException e){
            throw new IOException("There was an IO exception in the logg() method located in the Logger class. " + e.toString());
        } catch(Exception e){
            throw new Exception("There was an exception of unknown origin in the logg() method in the Logger class. " + e.toString());
        } finally{
            try{
                fileWriter.close();
            }catch(IOException e){
                throw new IOException("There was an IOException when closing the filewriter in the Logger class. " + e.toString());
            }
        }
    }
}