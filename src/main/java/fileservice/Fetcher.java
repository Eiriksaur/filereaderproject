package fileservice;

import java.io.File;
import java.io.IOException;
import loggservice.*;
import java.time.Duration;
import java.time.Instant;

public class Fetcher{

    String directory = "../src/main/resources";
    File folder = new File(directory);
    File[] fileList = folder.listFiles();
    Logger logger = new Logger();
    Instant funcStart;
    Instant funcEnd;
    String resString;

    public void getFilesInDir() throws Exception{
        //Simply collecting all the files in the directory and printing them to console
        try{
            funcStart = Instant.now();
            for(int i = 0; i < fileList.length; i++){
                System.out.println(fileList[i].getName());
            }
            funcEnd = Instant.now();    
            Duration exeTime = Duration.between(funcStart, funcEnd);
            resString = "The files in the dir is: ";
            for(int i = 0; i < fileList.length; i++){
                resString += fileList[i].getName() + " | ";
            }
            logger.logg(exeTime.toMillis(), resString);
        }catch(Exception e){
            throw new Exception("There was an Expection of uknown origin in the getFilesInDir() method. " + e.toString());
        }
    }

    public void getFilesByExtention() throws Exception{
        try{
            System.out.println("Name a fileextention to search for");
            String ext = System.console().readLine();
            funcStart = Instant.now();
            for(int i = 0; i < fileList.length; i++){
                int idx = fileList[i].getName().lastIndexOf('.');
                if (idx > 0) {
                    if(fileList[i].getName().substring(idx+1).equals(ext)) System.out.println(fileList[i].getName());
                }
            }
            funcEnd = Instant.now();    
            Duration exeTime = Duration.between(funcStart, funcEnd);
            resString = "The files in the dir whit the " + ext + " filextention is: ";
            for(int i = 0; i < fileList.length; i++){
                int idx = fileList[i].getName().lastIndexOf('.');
                if (idx > 0) {
                    if(fileList[i].getName().substring(idx+1).equals(ext)) resString += fileList[i].getName() + " | ";
                }
            }
            logger.logg(exeTime.toMillis(), resString);
        }catch(Exception e){
            throw new Exception("There was an Expection of uknown origin in the getFilesByExtention() method. " + e.toString());
        }
    }

    
}