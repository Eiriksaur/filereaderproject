package fileservice;

import java.io.File;
import java.io.FileReader;
import java.io.LineNumberReader;
import java.io.IOException;
import loggservice.*;
import java.time.Duration;
import java.time.Instant;

public class Reader{
    //A class presenting methods for simple reading of file properties as well as wordsearch methods.
    //Alter the 'filePath' variable to access different files
    String filePath = "../src/main/resources/Dracula.txt";

    File theFile = new File(filePath);
    Logger logger = new Logger();

    FileReader fileReader;
    LineNumberReader lineNumberReader;
    Instant funcStart;
    Instant funcEnd;

    public void getFileName() throws Exception, IOException{
        try{
            funcStart = Instant.now();
            String res = "The filename is: " + theFile.getName();
            funcEnd = Instant.now();    
            Duration exeTime = Duration.between(funcStart, funcEnd);
            logger.logg(exeTime.toMillis(), res);
            System.out.println(res);
        }catch(IOException e){
            throw new IOException("There was an IOException in the getFileName(). " + e.toString());
        }catch(Exception e){
            throw new Exception("There was an Exception of unknown origin in the getFileName(). " + e.toString());
        }
    }

    public void getFileSize() throws Exception, IOException{
        try{
            funcStart = Instant.now();
            String res = "The file is: " + theFile.length()/1024 + "KB";
            funcEnd = Instant.now();
            Duration exeTime = Duration.between(funcStart, funcEnd);
            logger.logg(exeTime.toMillis(), res);
            System.out.println(res);
        }catch(IOException e) {
            throw new IOException("There was an IOException in the getFileSize(). " + e.toString());
        }catch(Exception e) {
            throw new Exception("There was an Exception of unknown origin in the getFileSize(). " + e.toString());
        }
    }

    public void getNumberOfLines() throws Exception, IOException{
        try{
            funcStart = Instant.now();
            fileReader = new FileReader(filePath);
            lineNumberReader = new LineNumberReader(fileReader);
            while(lineNumberReader.skip(1000000) > 0){
                //Making sure that it keeps skiping until there are no more lines to skip.
            }
            int tempRes = lineNumberReader.getLineNumber()+1;
            String res = "There are " + tempRes + " number of lines in the file.";
            funcEnd = Instant.now();
            Duration exeTime = Duration.between(funcStart, funcEnd);
            logger.logg(exeTime.toMillis(), res);
            System.out.println(res);
        } catch(IOException e){
            throw new IOException("There was an IO-Exception in method getNumberOfLines. " + e.toString());
        } catch(Exception e){
            throw new Exception("There was an Exception of unknown origin in the getNumberOfLines. " + e.toString());
        } finally {
            try{
                fileReader.close();
            } catch(IOException e){
                throw new IOException("Failed while trying to close FileReader in the doesWordExist method. " + e.toString());
            }
        }
    }
    public void doesWordExist() throws Exception, IOException{
        try{
            System.out.println("Enter a words to search for");
            String searchTerm = System.console().readLine();
            funcStart = Instant.now();
            boolean wordExists = false;
            fileReader = new FileReader(filePath);
            lineNumberReader = new LineNumberReader(fileReader);
            String theLine = lineNumberReader.readLine();
            while(theLine != null && wordExists == false){
                /*Simple while loop that runs through every line in the file
                It breaks the line into single words and compares them all to the user input
                Finding a match or reaching the end of the file will exit the loop*/
                String[] words = theLine.split(" ");
                for(int i = 0; i < words.length; i++){
                    if(words[i].equals(searchTerm)){
                        wordExists = true;
                    }
                }
                theLine = lineNumberReader.readLine();
            }
            String res = wordExists ? "The word " + searchTerm + " exists" : "The word " + searchTerm + " does noe exist";
            funcEnd = Instant.now();
            Duration exeTime = Duration.between(funcStart, funcEnd);
            logger.logg(exeTime.toMillis(), res);
            System.out.println(res);
        }catch(IOException e){
            throw new IOException("There was an IOException in the doesWordExist method. " + e.toString());
        }catch(Exception e){
            throw new Exception("There was an Exception of unknown origin in the doesWordExist method. " + e.toString());
        } finally{
            try{
                fileReader.close();
            } catch(IOException e){
                throw new IOException("Failed while trying to close FileReader in the doesWordExist method. " + e.toString());
            }
        } 
    }
    public void findAllOccurancesOfWord() throws Exception, IOException{
        try{
            System.out.println("Enter a words to search for");
            String searchTerm = System.console().readLine().toLowerCase();
            funcStart = Instant.now();
            fileReader = new FileReader(filePath);
            lineNumberReader = new LineNumberReader(fileReader);
            String theLine = lineNumberReader.readLine();
            int wordCounter = 0;
            while(theLine != null){
                /*Simple while loop that runs through every line in the file
                It breaks the line into single words and compares them all to the user input
                Reaching the end of the file will exit the loop*/
                String[] words = theLine.toLowerCase().split(" ");
                for(int i = 0; i < words.length; i++){
                    if(words[i].equals(searchTerm)){
                        wordCounter++;
                    }
                }
                theLine = lineNumberReader.readLine();
            }
            String res = "The word: '" + searchTerm + "' occurs a total of " + wordCounter + " times.";
            funcEnd = Instant.now();
            Duration exeTime = Duration.between(funcStart, funcEnd);
            logger.logg(exeTime.toMillis(), res);
            System.out.println(res);
        }catch(IOException e){
            throw new IOException("There was an IOException in the doesWordExist method " + e.toString());
        }catch(Exception e){
            throw new Exception("There was an Exception of unknown origin in the doesWordExist method " + e.toString());
        } finally{
            try{
                fileReader.close();
            } catch(IOException e){
                throw new IOException("Failed while trying to close FileReader in the doesWordExist method " + e.toString());
            }
        } 
    }

}