import fileservice.*;
import java.io.IOException;

public class FileHandling{

    public static void moreOptions(){
        /*Displaying the file manipulation options until the user chooses to return to the start menu
        This method is called from the main method upon chosing a certain option from the start-menu*/

        boolean optionsRunning = true;

        Reader reader = new Reader();

        while(optionsRunning){
            try{
                System.out.println("Choose an option and enter the assosiated number: \n 1--> Get name of file " +
                "\n 2--> Get the file size \n 3--> Get how many lines the file has \n 4--> Search for spesific word \n " +
                "5--> Get number of occurances of a certain word \n 6--> Return to start-menu");
                int answer = Integer.parseInt(System.console().readLine());
                switch(answer){
                    case 1:
                        System.out.println("You chose 1");
                        reader.getFileName();
                        break;
                    case 2:
                        System.out.println("You chose 2");
                        reader.getFileSize();
                        break;
                    case 3:
                        System.out.println("You chose 3");
                        reader.getNumberOfLines();
                        break;
                    case 4:
                        System.out.println("You chose 4");
                        reader.doesWordExist();
                        break;
                    case 5:
                        System.out.println("You chose 5");
                        reader.findAllOccurancesOfWord();
                        break;
                    case 6:
                        System.out.println("Returning to start-menu");
                        optionsRunning = false;
                        break;
                    default:
                        System.out.println("Pls chose a number between 1-6");
                        break;
                }
            }
            catch(NumberFormatException e){
                System.out.println("Pls enter a number");
            }
            catch(Exception e){
                System.out.println(e.toString());
            }
        };
    };

    public static void main(String[] args){

        boolean running = true;

        Fetcher fetcher = new Fetcher();

        while(running){
        //Displaying the Start-menu until the user exits by choosing option 4
            try{
                System.out.println("Choose an option and enter the assosiated number: \n 1--> List all files" +
                "\n 2--> Get all files of a certain type \n 3--> File manipulation options \n 4--> Exit");
                int answer = Integer.parseInt(System.console().readLine());
                switch(answer){
                    case 1:
                        System.out.println("You chose 1");
                        fetcher.getFilesInDir();
                        break;
                    case 2:
                        System.out.println("You chose 2");
                        fetcher.getFilesByExtention();
                        break;
                    case 3:
                        System.out.println("You chose 3");
                        moreOptions();
                        break;
                    case 4:
                        System.out.println("Exiting program");
                        running = false;
                        break;
                    default:
                        System.out.println("Pls chose a number between 1-4");
                        break;
                }
            }
            catch (NumberFormatException e){
                System.out.println("Pls enter a number");
            }
             catch(Exception e){
                System.out.println(e.toString());
            }
            
        }
    };
};