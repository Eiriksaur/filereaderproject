# README

## Functionality
This program provides classes that lets you fetch files from a directory as well as extrating information about the files.

## Compile
Compile the program with the following command: "javac -d out src/main/java/*.java src/main/java/fileservice/*.java src/main/java/loggservice/*.java"
<img src="screenshots/classConvert.PNG" />

## JAR
Convert to .jar file with the following command: "jar cfe FileHandling.jar FileHandling *.class fileservice/*class loggservice/*.class"
<img src="screenshots/jarConvert.PNG" />

## Running
Run the program with the following command: "java -jar FileHandling.jar"
<img src="screenshots/runningJar.PNG" />
